
package com.mycompany.palabrapalindrome;

import java.util.Scanner;


public class PalabraPalindrome {

    public static void main(String[] args) {
      
        Scanner s = new Scanner(System.in);
        
        String palabra;
        char [] palindrome;
        int izq, der;
        System.out.print("ingrese la palabra: ");
        palabra = s.nextLine();
        palabra = palabra.toLowerCase();
        palabra = palabra.replace(" ","");
        palindrome = palabra.toCharArray(); 
        
        izq = 0;
        der = palindrome.length-1;
        
            while(izq<der){
               if(palindrome[izq]==palindrome[der]){
                   der--;
                   izq++;
            }else{
                System.out.print("La palabra no es un palindrome");
                break;
            }
        }
        
                if(izq==der){
            System.out.print("La palabra es un palindrome");
        }
    }
}
