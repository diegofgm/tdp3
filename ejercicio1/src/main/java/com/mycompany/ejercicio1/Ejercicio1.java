
package com.mycompany.ejercicio1;

public class Ejercicio1 {

    public static void main(String[] args) {
        
        int [] vector = new int[]{0,0,1};
        tribonacci(vector, 9); 
    }
    
      public static void tribonacci(int [] vector, int secuencia){
        
        
         if (secuencia == 0){
            
            System.out.println("0");
            return;
            
        }else if(secuencia<vector.length){
            
            System.out.print("{");
            for(int i=0; i<secuencia; i++){
                System.out.print(vector[i]+",");
            }
            System.out.print("}");
            
        }else{
            
            int suma=0;           
            System.out.print("Tribonacci: "+ vector[0] + vector[1] + vector[2]);
            
            for(int i=3;i<secuencia;i++){
                suma = vector[0] + vector[1] + vector[2];
                System.out.print(","+suma);
                vector[0] = vector[1];
                vector[1] = vector[2];
                vector[2] = suma;
            }    
        }
    }    
}
